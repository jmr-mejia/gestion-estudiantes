from django.contrib import admin
from .models import Student, Team, Evaluation, Calification

admin.site.register(Student)
admin.site.register(Team)
admin.site.register(Evaluation)
admin.site.register(Calification)
