# Django
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import User

class Team(models.Model):
    """ Team base model. Can update with other team"""
    name       = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'Team'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.name}'


class Evaluation(models.Model):
    """ Evaluation base model. """
    name       = models.CharField(max_length=40)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'Evaluation'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.name}'


class Student(User):
    """ Students base model """

    DS = "Data Science"
    BE = "Backend"
    FE = "Frontend"

    BRANCHES = [
        ("Data Science", "Data Science"),
        ("Backend", "Backend"),
        ("Frontend", "Frontend"),
    ]
    branch      = models.CharField(max_length=12, choices=BRANCHES, blank=True)
    cohort      = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(8)], default=1)
    team_name   = models.ForeignKey(Team, on_delete=models.CASCADE, blank=True)
    evaluations = models.ManyToManyField(Evaluation, blank=True)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'Student'
        ordering = ['-created_at']
    
    def __str__(self):
        return f'{self.first_name} {self.last_name}, team: {self.team_name.name}'


class Calification(models.Model):
    """ Calification base model """
    name         = models.ForeignKey(Evaluation, on_delete=models.CASCADE, blank=False)
    student_name = models.ForeignKey(Student, on_delete=models.CASCADE, blank=False)
    score        = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(100.0)], default=0.0)
    created_at   = models.DateTimeField(auto_now_add=True)
    updated_at   = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'Calification'
        ordering = ['-created_at']
    
    def __str__(self):
        return f'{self.name}, student: {self.student_name.first_name}'