# Django
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Models
from .models import Team, Student, Evaluation

def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            team = Team.object.filter(user=user)
            evaluations = Evaluation.object.filter(user=user)
            context = {
                'user': user,
                'team': team,
                'evaluation': evaluations,
            }
            return redirect('home', context)
        else:
            context = {'message': 'Wrong username or password'}
            return render(request, 'login.html', context)
    else:
        return render(request, 'login.html')


@login_required
def logout_view(request):
    logout(request)
    return redirect('login')


@login_required
def home_view(request):
    if request.method == 'GET' and request.user is not None:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        team = Team.object.filter(user=user)
        evaluations = Evaluation.object.filter(user=user)
        context = {
            'user': user,
            'team': team,
            'evaluation': evaluations,
        }
        return render(request, 'home.html', context)

